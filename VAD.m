% VAD Freq Band

% Test SS & HR Method using VAD by Frequency Band

clc
clear

% Select Test Data
speech = "02";
noise_type = "white";
snr_level = ["0" "5" "10" "15"];

%Add Audio Path
addpath('/Users/napatr.tan/Documents/MATLAB/SeniorProject/Input')

% System Parameter
frameLength = 256;                  % frame length
npoint = 512;                       % n-point of fft
sp = 0.5;                           % percent of overlap
LengthNO = sp*frameLength;          % overlap frame
LengthO = frameLength - LengthNO;   % no overlap frame


% Import Clean Speech
im_file = "sp"+speech+".wav";
[signal_c, Fs_c] = audioread(im_file);

% Signal Parameter
T_c = 1/Fs_c;                                   % Sampling period                    
L=length(signal_c);                             % Length of signal
nframe = floor(L/(frameLength*(1-sp)))-1;       % no. of segment frame
winH = hamming(frameLength);                    % Create hamming window
winGain = LengthO/sum(winH);
    
for j = 1:size(snr_level,2)

    % Import Noisy Signal
    noisy_file = "sp"+speech+"_"+noise_type+"_sn"+snr_level(j)+".wav";
    [signal, Fs] = audioread(noisy_file);
    
    signal_vad = signal - mean(signal);
    
    % Initial Frame Window
    currentFrame = 1;
    nextFrame = currentFrame + frameLength-1;

    % Create Empty VAD Output
    outVAD = zeros(1,nframe);
    VAD_manual = zeros(1,nframe);
    en_total = zeros(nframe,1);
    en_lb = zeros(nframe,1);
    en_hb = zeros(nframe,1);
    delta_en = zeros(1,nframe);

    % Manual VAD
    for n = 1:nframe

        % Current Frame Signal
        signal_d = signal_vad(currentFrame:nextFrame);
        clean_signal = signal_c(currentFrame:nextFrame);

        % Collect Current Signal Data
        signal_DFT = fft(abs(signal_d.*winH),npoint);
        signal_DFT = max(signal_DFT,0);

        % Calculate Signal Power
        en_total(n) = norm(signal_DFT)^2; %Faster
        en_lb(n) = norm(signal_DFT(1:end/2))^2;
        en_hb(n) = norm(signal_DFT(end/2+1:end))^2;

        % Find Different Energy
        delta_energy = en_lb(n) - en_hb(n);

        % First Order Recursive Filter to Smooth Envelop
        if n == 1
            delta_en = 0;
        else
            delta_en(n) = 0.5*delta_energy + 0.5*delta_en(n-1);
        end

        % Check VAD Manual
        signal_energy = norm(clean_signal);
        if signal_energy > 0.2
            decision = 1;
        else
            decision = 0;
        end

        % Append Decision to Output Matrix
        VAD_manual(n) = decision;

        % Update to Next Frame
        currentFrame = nextFrame-sp*frameLength+1;
        nextFrame = currentFrame + frameLength-1;

    end

    % Judgement VAD
    for i = 1:nframe

        if abs(delta_en(i)) > 1e-6 && en_total(i) > mean(en_total)/(0.5*str2double(snr_level(j))+0.7)
            
            outVAD(i) = 1;
            
        else
            
            outVAD(i) = 0;
            
        end

    end
    
    % Filter VAD Output
    for l = 2:nframe-1

        if outVAD(l-1) == 1 && outVAD(l+1) == 1 && outVAD(l) == 0
            outVAD(l) = 1;
        end

    end

    vad_acc = sum(VAD_manual==outVAD)/nframe;


    % Plot VAD Result
    vad_graph = figure;
    sgtitle("VAD Result Speech "+speech+", "+noise_type+" noise SNR "+snr_level(j)+" dB")
    subplot(3,1,1)
    plot((1:length(signal_c))*T_c,signal_c)
    hold on
    plot((1:length(VAD_manual))*sp*frameLength/Fs_c,VAD_manual*max(signal_c),'r')
    ylabel('Y(t)')
    xlabel('Time (s)')
    xlim([0,length(signal_c)*T_c])
    ylim([-0.4,0.4])
    title('(a) Clean Signal')

    subplot(3,1,2)
    plot((1:length(signal))*T_c,signal)
    ylabel('Y(t)')
    xlabel('Time (s)')
    xlim([0,length(signal_c)*T_c])
    ylim([-0.4,0.4])
    title('(b) Noisy Signal')

    subplot(3,1,3)
    plot((1:length(VAD_manual))*sp*frameLength/Fs_c,VAD_manual,'--')
    hold on
    plot((1:length(outVAD))*sp*frameLength/Fs_c,outVAD,'-')
    ylabel('VAD Result')
    xlabel('Time (s)')
    title("(c) VAD Result")
    legend('VAD Manual','VAD by Frequency Band')
    xlim([0,length(signal_c)*T_c])
    ylim([0,2])
    
end