function spectrogram_plot_sp(y,fftlen,Fs,text)

    [~,F,T,P] = spectrogram(y,fftlen,0.5*fftlen,512,Fs);
    surf(T,F,10*log10(abs(P)),'EdgeColor','none');
    axis xy; 
    axis tight; 
    colormap(jet)
%     colormap('bone'); 
    view(0,90);
    xlabel('Time (s)'); 
    ylabel('Frequency (Hz)');
    title(text)
    colorbar;
    
end