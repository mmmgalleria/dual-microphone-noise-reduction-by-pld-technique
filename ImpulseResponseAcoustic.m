% Plot of Impulse Response

clear
close all

% System Parameter
period = 0.032;                 % Period 32 ms
delay = 0.002;                  % Delay 2 ms
T = 1/8000;                      % Sampling Period

 % Random Impulse
whiteNoise1 = randn(1,period/T);
whiteNoise2 = randn(1,period/T);
h1 = whiteNoise1.*exp(-150*(0:period/T-1)*T);
h2 = whiteNoise2.*exp(-150*(0:period/T-1)*T);

stem((1:length(h1))*T,h1,'b','LineWidth',1)
xlabel('Time (s)')
ylabel('Amplitude')
title('Impulse Response of Acoustic Transfer Function')