# Dual-Microphone-Noise-Reduction
This README file provides instructions for using the dual-microphone noise reduction for mobile phone in noisy environment by __Power Level Different Technique (PLD)__ MATLAB project. The project aims to reduce noise in audio recordings using dual microphones. By following these steps, you will be able to set up the project, run the noise reduction algorithm, and process your audio recordings.
## Difference between First and Second Microphone
Audio Input in the Second Microphone is affected by Impulse Respponse
![Impulse Response](./FilterImpulseResponse.jpg)
## Harmonic Regeneration
This Project also featured with Harmonic Regeneration to improve cleaner speech
## SNR
The result have been indicated result by __SNR (Signal to Noise Ratio)__. If SNR increased, the signal will be more clean.
## Prerequisites
Before you begin, ensure that you have the following software and resources available:

- __MATLAB__: Make sure you have MATLAB installed on your system. You can download MATLAB from the official MathWorks website: MATLAB.
- __Dual Microphone Audio Recordings__: You will need a set of dual microphone audio recordings that contain both the desired audio signal and the noise you want to reduce. These recordings should be in a suitable audio file format, such as WAV or MP3.

## Setup Instructions
Follow these steps to set up and use __the Dual Microphone Noise Reduction MATLAB project__:
- __Clone the project files__: Download the project files from the project repository or obtain them from the source you're working with.
- __Launch MATLAB__: Open MATLAB on your system.
- __Navigate to this project directory__: In MATLAB, navigate to the directory where you have downloaded or saved the Dual Microphone Noise Reduction project files.
- __Run the MATLAB script__: Open the main MATLAB script file, typically named PLD_Noise_Estimator.m, in the MATLAB editor.
- __Process the audio recordings__: Utilize the provided noise reduction algorithm or methods within the script to process the audio recordings and reduce the noise. The specific implementation may vary depending on the project files you have obtained.
- __Save the processed audio__: After the noise reduction process, you can use the audiowrite function to save the processed audio to a new file.

## Result 
The results will be shown as a Spectrogram Plot compared between Original Speech, Noise Affected Speech and Noise Reduction Speech.
![Todo](./Spectrogram_Plot/ResultSpectrogramsp01babble0.jpg)

## Additional Resources

For more information and detailed documentation on MATLAB and audio processing, refer to the following resources:

- __MATLAB Documentation__: The official MATLAB documentation provides comprehensive information on MATLAB functions, scripts, and toolboxes.

- __Video Presentation__ : https://youtu.be/XaCVEO0YsJc
