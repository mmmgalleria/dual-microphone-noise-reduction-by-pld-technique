% Create AWGN Noise Signal

clc
clear

% Select Test Data
speech = ["01" "02" "03" "04" "05" "06" "07" "08" "09" "10"];
snr_level = ["0" "5" "10" "15"];

% Add Audio Path
addpath('/Users/napatr.tan/Documents/MATLAB/SeniorProject/Input')

for i = 1:size(speech,2)
    
    % Import Clean Signal
    im_file = "sp"+speech(i)+".wav";
    [signal_c, Fs_c] = audioread(im_file);

    for j = 1:size(snr_level,2)

        out = awgn(signal_c,str2double(snr_level(j)),'measured');
        audiowrite("/Users/napatr.tan/Documents/MATLAB/SeniorProject/Input/sp"+speech(i)+"_white_sn"+snr_level(j)+".wav",out,Fs_c);

    end

end